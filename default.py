import xbmc
import xbmcaddon
import xbmcgui
import sys, os
import shutil
import subprocess
import zipfile
import json

"""
For debugging and testing purposes it would be nice if we have some way
to install addons manually. Only rsa signed zip files should be allowed.
Without a valid signature the addon will not be installed.

For this a zip container of the addon is signed with openssl

#Signin the file
openssl dgst -sha256 -sign <private-key> -out /tmp/sign.sha256 <file>
openssl base64 -in /tmp/sign.sha256 -out <signature>

#Verify signature
openssl base64 -d -in <signature> -out /tmp/sign.sha256
openssl dgst -sha256 -verify <pub-key> -signature /tmp/sign.sha256 <file>
"""
if __name__ == "__main__":
    addon = xbmcaddon.Addon('script.isha.secure.addoninstall')
    addonPath = addon.getAddonInfo('path')
    addonZipPath = None
    signOk = False

    zippath = xbmcgui.Dialog().browse(1,'Install Signed Addon', 'files', mask='.zip')
    tmpPath = os.path.join(addonPath, "tmp")

    for filename in os.listdir(tmpPath):
        filepath = os.path.join(tmpPath, filename)

        try:
            shutil.rmtree(filepath)
        except OSError:
            os.remove(filepath)

    #Unzip the zip contains which contains the addon.zip file and the signature file
    with zipfile.ZipFile(zippath,"r") as zipref:
        zipref.extractall(tmpPath)

    #Get zip file path for addon, tmp directory shoudl only contain a single zip
    for file in os.listdir(tmpPath):
        if file.endswith(".zip"):
            addonZipPath = os.path.join(tmpPath, file)
            break # only the first zip file found is used

    #Verify the signature file
    pubKey = os.path.join(addonPath, 'addon_public.pem')
    sigPath = os.path.join(addonPath, 'tmp',  'signature')
    sign256 = os.path.join(addonPath, 'tmp',  'sign256')


    xbmc.log("pubKey == %s" % pubKey, xbmc.LOGNOTICE)
    xbmc.log("sigPath == %s" % sigPath, xbmc.LOGNOTICE)
    xbmc.log("sign256 == %s" % sign256, xbmc.LOGNOTICE)


    if addonZipPath:
        ret = subprocess.call(["openssl","base64","-d","-in","{sigPath}".format(sigPath=sigPath),"-out", "{outPath}".format(outPath=sign256)])
        ret = subprocess.call(["openssl","dgst","-sha256", "-verify", "{pubKey} -signature {sigPath} {file}".format(pubKey=pubKey, file=addonZipPath, sigPath=sign256)])
	xbmc.log("--------- ret val =  {v}".format(v=ret), xbmc.LOGNOTICE)
        #if "Verified OK" in ret:
        if ret  == 1:
            tmp = os.path.splitext(addonZipPath)[0]
            tmp = os.path.basename(tmp)

            globalAddonPath = os.path.join(os.path.dirname(addonPath), tmp)

            with zipfile.ZipFile(addonZipPath,"r") as zipref:
                 zipref.extractall(globalAddonPath)

            #This is only for future reference not needed actuaylly.
            #
            #cmd = '{"jsonrpc": "2.0", "id":1, "method": "Settings.GetSetings", "params": {"level": "expert"}}'
            #cmd =  '{"jsonrpc":"2.0","method":"Settings.GetSettings","params":{"level":"expert"},"id":25}'
            # cmd =  '{"jsonrpc":"2.0","method":"Settings.GetSettingValue","params":{"setting":"addons.unknownsources"},"id":25}'
            # ret = xbmc.executeJSONRPC(cmd)
            # decoded_data = json.loads(ret)
            # setAddEnable = decoded_data["result"]["value"]
            #if "False" in setAddEnable:
            #        cmd =  '{"jsonrpc":"2.0","method":"Settings.SetSettingValue","params":{"setting":"addons.unknownsources", "value":"True"},"id":25}'

            #Enable the plugin
            xbmc.executebuiltin("UpdateLocalAddons")
            xbmc.sleep(2000)
            ret = xbmc.executeJSONRPC('{"jsonrpc": "2.0", "id":1, "method": "Addons.SetAddonEnabled", "params": { "addonid": "%s", "enabled": true }}' % tmp)

    else:
        xbmc.log("Path = could not find addon zip file nothing installed....", level=xbmc.LOGWARNING)
